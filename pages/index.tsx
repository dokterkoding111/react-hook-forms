import Image from "next/image";
import { Inter } from "next/font/google";
import FormInput from "@/src/components/formInput";
import { Bookmark, Person, Phone, Stars } from "@mui/icons-material";
import { useForm } from "react-hook-form";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {

  const {register, handleSubmit} = useForm()

  return (
    <div className="flex flex-col gap-5 justify-center py-10 items-center">
      <p className="text-4xl">React Hook Forms</p>
      <div>
        <form>
          <p className="capitalize text-xl mb-2">Fullname*</p>
          <FormInput name="fullname" icon={<Person />} />

          <p className="capitalize text-xl mb-2">Address*</p>
          <FormInput name="fullname" icon={<Bookmark />} />

          <p className="capitalize text-xl mb-2">Phone*</p>
          <FormInput name="fullname" icon={<Phone />} />

          <p className="capitalize text-xl mb-2">Hobby*</p>
          <FormInput name="fullname" icon={<Stars />} />

          <button className="bg-green-500 p-3 w-full rounded-md mt-5 text-lg font-medium text-white">
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}
