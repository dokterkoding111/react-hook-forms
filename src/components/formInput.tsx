interface inputType {
  name: string;
  icon: any;
  // ref : any,
}

const FormInput = ({ name, icon }: inputType) => {
  return (
    <div className="relative flex h-12 bg-slate-200 rounded-md justify-center mb-3">
      <div className="absolute left-0 top-0 bottom-0 flex items-center pl-2">{icon}</div>
      <input
        className="bg-transparent w-full focus:outline-none pl-12"
        name={name}
      />
    </div>
  );
};

export default FormInput;
